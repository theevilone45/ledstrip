import board
import neopixel
from time import sleep

GLOBAL_SEGMENTS_COUNT = 14
GLOBAL_LED_STRIP = neopixel.NeoPixel(board.D18, GLOBAL_SEGMENTS_COUNT, brightness=1)
print("Initialized {} LED segments".format(len(GLOBAL_LED_STRIP)))


def fuck_the_police(pixels):
    try:
        while True:
            pixels.fill((255, 0, 0))
            pixels.show()
            sleep(0.25)
            pixels.fill((0, 255, 0))
            pixels.show()
            sleep(0.25)
            pass
    except KeyboardInterrupt:
        if pixels is not None:
            pixels.fill((0, 0, 0))
            pixels.show()
        else:
            print("LED are not successfully connected")
    pass


def nice_mood(pixels):
    pixels.fill((218, 154, 68))
    pixels.show()


def single_segment_floating(pixels, color):
    it = 0
    try:
        while True:
            if it >= GLOBAL_SEGMENTS_COUNT:
                it = 0
            pixels.fill((0, 0, 0))
            pixels[it] = color
            pixels.show()
            it += 1
            sleep(0.2)
            pass
    except KeyboardInterrupt:
        if pixels is not None:
            pixels.fill((0, 0, 0))
            pixels.show()
        else:
            print("LED are not successfully connected")
    pass


def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        r = g = b = 0
    elif pos < 85:
        r = int(pos * 3)
        g = int(255 - pos * 3)
        b = 0
    elif pos < 170:
        pos -= 85
        r = int(255 - pos * 3)
        g = 0
        b = int(pos * 3)
    else:
        pos -= 170
        r = 0
        g = int(pos * 3)
        b = int(255 - pos * 3)
    return r, g, b


def rainbow_cycle(pixels, wait):
    try:
        while True:
            for j in range(255):
                for i in range(GLOBAL_SEGMENTS_COUNT):
                    pixel_index = (i * 256 // GLOBAL_SEGMENTS_COUNT) + j
                    pixels[i] = wheel(pixel_index & 255)
                pixels.show()
                sleep(wait)
    except KeyboardInterrupt:
        if pixels is not None:
            pixels.fill((0, 0, 0))
            pixels.show()
        else:
            print("LED are not successfully connected")


if __name__ == '__main__':
    #fuck_the_police(GLOBAL_LED_STRIP)
    # nice_mood(GLOBAL_LED_STRIP)
    single_segment_floating(GLOBAL_LED_STRIP, (125, 125, 0))
    #rainbow_cycle(GLOBAL_LED_STRIP, 0.001)
